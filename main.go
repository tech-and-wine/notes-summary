package main

import (
	"fmt"
	"log"
	"os"

	"github.com/parakeet-nest/parakeet/completion"
	"github.com/parakeet-nest/parakeet/llm"
	"github.com/xanzy/go-gitlab"
)

func main() {

	gitLabCli, gitLabCliErr := gitlab.NewClient(os.Getenv("GITLAB_TOKEN"), gitlab.WithBaseURL("https://gitlab.com"))
	if gitLabCliErr != nil {
		log.Fatal("😡 Failed to create client:", gitLabCliErr)
	}

	// https://gitlab.com/gitlab-org/gitlab/-/issues
	//https://gitlab.com/api/v4/projects/278964/issues/443347/notes
	projectId := 278964
	issueId := 443347
	notes, _, gitLabNotesErr := gitLabCli.Notes.ListIssueNotes(projectId, issueId, nil)

	if gitLabNotesErr != nil {
		log.Fatal("😡 Failed to get the notes:", gitLabNotesErr)
	}

	ollamaUrl := "http://localhost:11434"
	model := "phi3:mini"
	//model := "phi3:medium"

	systemContent := `You are an helpful agent. 
	Your duty is to provide a clear and concise summary of the notes provided with the context.
	each note is surrounded by <note> and </note> tags.`

	var contextContent = `<context>`
	for _, note := range notes {
		contextContent += `<note>` + note.Body + `</note>`
		//fmt.Println(note.Body)
	}
	contextContent += `</context>`

	userContent := `Make summary of the notes provided with the context.`

	options := llm.Options{
		Temperature: 0.5, // default (0.8)
		RepeatLastN: 2,   // default (64) the default value will "freeze" deepseek-coder
		RepeatPenalty: 1.5,
	}

	query := llm.Query{
		Model: model,
		Messages: []llm.Message{
			{Role: "system", Content: systemContent},
			{Role: "system", Content: contextContent},
			{Role: "user", Content: userContent},
		},
		Options: options,
	}

	_, chatErr := completion.ChatStream(ollamaUrl, query,
		func(answer llm.Answer) error {
			fmt.Print(answer.Message.Content)
			return nil
		})

	if chatErr != nil {
		log.Fatal("😡:", chatErr)
	}


}
